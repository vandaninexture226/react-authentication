import React, { useEffect } from 'react'
import { useNavigate } from 'react-router-dom';
import Button from '@mui/material/Button';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Home() {
    let navigate = useNavigate();

    const handleLogout = () => {
        sessionStorage.removeItem('Auth Token');
        navigate('/login')
        toast.success('Successfully Log Out..!');
    }

    useEffect(() => {
        let authToken = sessionStorage.getItem('Auth Token')

        if(authToken) {
            navigate('/home')
        }

        if(!authToken) {
            navigate('/login')
        }
    },[])

    return (
        <div>
            <ToastContainer />
            <div className="heading-container">
                Home Page
                <Button onClick={handleLogout} variant="contained"> Log out</Button>
            </div>
        </div>
    )
}