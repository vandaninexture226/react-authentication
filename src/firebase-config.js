import { initializeApp } from "firebase/app";

const firebaseConfig = {
    apiKey: "AIzaSyB6vd-vZIAug1hAFcBHb10XYZrI4z4JZmI",
    authDomain: "react-firebase-v9-2b0aa.firebaseapp.com",
    projectId: "react-firebase-v9-2b0aa",
    storageBucket: "react-firebase-v9-2b0aa.appspot.com",
    messagingSenderId: "179749172413",
    appId: "1:179749172413:web:3dc2e8c10f0ca7dedfb3d4",
    measurementId: "G-K8H3FCLDPY"
};

export const app = initializeApp(firebaseConfig);